from webvtt import WebVTT, Caption
from datetime import timedelta


# an example of a payload:
# {
#     "payload": {
#         "en": {
#             "payload": [
#                 {
#                     "id": 0,
#                     "start": 43.22,
#                     "end": 54.36,
#                     "text": "translation of first segment as string"
#                 },
#                 {
#                     "id": 1,
#                     "start": 55.22,
#                     "end": 57.4,
#                     "text": "translation of second segment as string"
#                 } ...
#             ],
#             "success": true
#         }
#     },
#     "type": "translations",
#     "success": true
# }

def get_time_in_vtt_format(time: float):
    ms_time = str(time).split(".")[-1]
    if len(ms_time) < 3:
        for i in range(3 - len(ms_time)):
            ms_time = ms_time + '0'
    return str(timedelta(seconds=int(time))) + "." + ms_time

# prepare vtt files with captions for every language
def whisper_translations_output_to_vtt(whisper_translations_output):
    translations_as_vtt = {}
    if not whisper_translations_output["success"]:
        translations_as_vtt["success"] = False
        return translations_as_vtt

    translations_as_vtt["success"] = True
    translations_as_vtt["payload"] = {}
    for target_language_id in whisper_translations_output["payload"]:
        translations_as_vtt["payload"][target_language_id] = {}
        if not whisper_translations_output["payload"][target_language_id]["success"]:
            translations_as_vtt["payload"][target_language_id]["success"] = False
            continue
        translations_as_vtt["payload"][target_language_id]["payload"] = WebVTT()
        translations_as_vtt["payload"][target_language_id]["success"] = True
        for segment in whisper_translations_output["payload"][target_language_id]["payload"]:
            # used this changing comma period to https://github.com/openai/whisper/discussions/98#discussioncomment-3725983
            # might be better to just use the whole script and stick to srt but it seems like this should also work
            start_time = get_time_in_vtt_format(segment['start'])
            end_time = get_time_in_vtt_format(segment['end'])
            caption = Caption(
                start_time,
                end_time,
                segment["text"]
            )
            translations_as_vtt["payload"][target_language_id]["payload"].captions.append(caption)
    return translations_as_vtt