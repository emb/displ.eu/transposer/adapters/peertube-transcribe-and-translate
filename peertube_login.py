import requests
import time
from requests.exceptions import HTTPError


class PeerTubeLoginException(Exception):
    pass

class LoginPeerTube:
    def __init__(self, connector, basedomain, admin_username, admin_password):
        self.access_token_valid_until = 0
        self.refresh_token_valid_until = 0
        self.connector = connector
        self.basedomain = basedomain
        self.admin_username = admin_username
        self.admin_password = admin_password
        self.login("password")

    def get_access_token(self):
        if self.is_valid(self.access_token_valid_until):
            return self.access_token
        else:
            if self.is_valid(self.refresh_token_valid_until):
                print("using refresh token")
                return self.login(login_type="refresh_token")
            else:
                print("login with password")
                return self.login(login_type="password")

    def login(self, login_type):
        try:
            response_client = requests.get(
                f"{self.basedomain}/api/v1/oauth-clients/local"
            )
            if response_client.status_code != 200:
                response_client.raise_for_status()

            response_client_json = response_client.json()
            token_payload = {
                "client_id": response_client_json["client_id"],
                "client_secret": response_client_json["client_secret"],
                "grant_type": login_type,
            }
            if login_type == "password":
                token_payload["username"] = self.admin_username
                token_payload["password"] = self.admin_password
            if login_type == "refresh_token":
                token_payload["refresh_token"] = self.refresh_token
            response_token = requests.post(
                f"{self.basedomain}/api/v1/users/token", data=token_payload
            )
            if response_token.status_code != 200:
                response_token.raise_for_status()

            response_token_json = response_token.json()
            self.access_token = response_token_json["access_token"]
            self.refresh_token = response_token_json["refresh_token"]
            self.access_token_valid_until = (
                int(time.time()) + response_token_json["expires_in"]
            )
            self.refresh_token_valid_until = (
                int(time.time()) + response_token_json["refresh_token_expires_in"]
            )
            return self.access_token
        except HTTPError as http_error:
            result = {
                "status": http_error.response.status_code,
                "message": str(http_error),
            }
            raise PeerTubeLoginException(f"Login to peertube failed {result}")
        except Exception as e:
            result = {
                "status": 500,
                "message": str(e),
            }
            raise PeerTubeLoginException(f"Login to peertube failed {result}")

    @staticmethod
    def is_valid(valid_until):
        return int(time.time()) < valid_until

