cd
mkdir peertube-transcribe-and-translate
cd peertube-transcribe-and-translate
git clone https://git.fairkom.net/emb/displ.eu/transposer/service-connector
git clone https://git.fairkom.net/emb/displ.eu/transposer/shared-lib.git
cd shared-lib
cd modules
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/utils
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/adapter-utils
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/database.git
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/whisper-data-transformer.git
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/html-parser.git
cd ../adapters
git clone https://git.fairkom.net/emb/displ.eu/transposer/adapters/peertube-transcribe-and-translate.git
git clone https://git.fairkom.net/emb/displ.eu/transposer/adapters/base-translate-async.git
cd ../../service-connector
python3 -m venv ./venv
source venv/bin/activate
pip install -r requirements.txt
pip install -r ../shared-lib/modules/utils/requirements.txt
pip install -r ../shared-lib/modules/database/requirements.txt
pip install -r ../shared-lib/modules/adapter-utils/requirements.txt
pip install -r ../shared-lib/modules/whisper-data-transformer/requirements.txt
pip install -r ../shared-lib/modules/html-parser/requirements.txt
pip install -r ../shared-lib/adapters/peertube-transcribe-and-translate/requirements.txt
pip install -r ../shared-lib/adapters/base-translate-async/requirements.txt
