# import the baseclass for the adapter
# this class loads parses the config
# file at config_path
import basetranslateasyncadapter

# import the modules you need for your
# adapter implementation
import requests
import logging
import os
from requests.exceptions import HTTPError
import uuid
from dotenv import load_dotenv

# self-written modules
from adapter_utils import log_error, get_translation_targets, get_supported_languages
from captions_utils import whisper_translations_output_to_vtt
import peertube_login

PEERTUBE_BASEDOMAIN_PROD = "peertube_basedomain_prod"
PEERTUBE_BASEDOMAIN_DEV = "peertube_basedomain_dev"
ADMIN_USERNAME = "peertube_admin_username"
ADMIN_PASSWORD = "peertube_admin_password"
EXTERNAL_ENDPOINT_BASEDOMAIN = "external_endpoint_basedomain"
INTERNAL_ENDPOINT_BASEDOMAIN = "internal_endpoint_basedomain"
REQUIRED_CONFIG_PARAMS = [
    ADMIN_USERNAME,
    ADMIN_PASSWORD,
    EXTERNAL_ENDPOINT_BASEDOMAIN,
]
SOURCE_ID = "source_id"

TRANSCRIBE_AND_TRANSLATE_COLLECTION = "transcribe_and_translate"

class MissingConfigException(Exception):
    pass


class NoTargetLanguagesLeftToTranslateException(Exception):
    pass



class Adapter(basetranslateasyncadapter.BaseTranslateAsyncAdapter):
    def __init__(self, connector, config_path):
        super(Adapter, self).__init__(connector, config_path)
        print("Peertube Caption Translate Adapter ... config:", self.config)

        for config_param in REQUIRED_CONFIG_PARAMS:
            if config_param not in self.config.get("peertube_transcribe_and_translate").options:
                raise MissingConfigException(
                    f"required config {config_param} is missing"
                )
        # https://stackoverflow.com/questions/1793261/how-to-join-components-of-a-path-when-you-are-constructing-a-url-in-python
        # This is why I am not using os.path.join and just removing the / at the end.
        self.peertube_basedomain_prod = self.config.get("peertube_transcribe_and_translate").get(PEERTUBE_BASEDOMAIN_PROD, None)
        self.peertube_basedomain_dev = self.config.get("peertube_transcribe_and_translate").get(PEERTUBE_BASEDOMAIN_DEV, None)

        self.supported_language_ids = get_supported_languages(self.config.get("peertube_transcribe_and_translate").get(EXTERNAL_ENDPOINT_BASEDOMAIN).rstrip("/"))

        if self.peertube_basedomain_prod is not None:
            self.login_peer_tube_prod = peertube_login.LoginPeerTube(
                self.connector,
                self.peertube_basedomain_prod.rstrip("/"),
                self.config.get("peertube_transcribe_and_translate").get(ADMIN_USERNAME),
                self.config.get("peertube_transcribe_and_translate").get(ADMIN_PASSWORD),
            )

        if self.peertube_basedomain_dev is not None:
            self.login_peer_tube_dev = peertube_login.LoginPeerTube(
                self.connector,
                self.peertube_basedomain_dev.rstrip("/"),
                self.config.get("peertube_transcribe_and_translate").get(ADMIN_USERNAME),
                self.config.get("peertube_transcribe_and_translate").get(ADMIN_PASSWORD),
            )

        if self.peertube_basedomain_prod is None:
            raise MissingConfigException(f"either PEERTUBE_BASEDOMAIN_PROD or PEERTUBE_BASEDOMAIN_DEV is required")

        self.translation_target_topics = get_translation_targets(self.config.get("peertube_transcribe_and_translate").get(INTERNAL_ENDPOINT_BASEDOMAIN).rstrip("/"))
        self.peertube_collection = self.set_up_database()

    def run(self, payload, msg):
        result = {}
        success = False
        print("This is the peertube transcribe and translate adapter")
        # if payload is None return an error maybe?
            
        try:
            if 'source_id' in payload:
                logging.info(f"payload: {payload}")
                source_id = payload[SOURCE_ID]
                logging.info(f"payload: {source_id}")
                # TODO need to handle case where we say we want to use whisper even though there was at least one caption
                # then we can pass the target_language_ids here instead of depending on the whisper output
                source_id_document = self.get_data_by_source_id(source_id)
                video_id = source_id_document["video_id"]
                self.peertube_basedomain = source_id_document["peertube_basedomain"]
                match self.peertube_basedomain:
                    case self.peertube_basedomain_prod:
                        self.login_peer_tube = self.login_peer_tube_prod
                    case self.peertube_basedomain_dev:
                        self.login_peer_tube = self.login_peer_tube_dev
                    case _:
                        result["success"] = success
                        return result
                # TODO get more from document like requestURL, api, ...
                translations = source_id_document["translations"]
                translations_as_vtt = whisper_translations_output_to_vtt(translations)
                if translations_as_vtt["success"]:
                    translations_as_vtt = translations_as_vtt["payload"]
                    #  if this is flase how do I skip to the end programatically???
                first_part_file_name = str(uuid.uuid4())
                # TODO better not delete but update the status to done or at least to recieved on the peertube adapter side
                # self.peertube_collection.delete_one({"source_id": payload[SOURCE_ID]})
            else:
                if "videoId" not in payload or "peertubeInstanceBaseDomain" not in payload:
                    log_error(self.connector, 400, "no videoId or peertubeInstanceBaseDomain in payload", result=result)
                    result["success"] = success
                    return result
                
                video_id = payload["videoId"]
                self.peertube_basedomain = payload["peertubeInstanceBaseDomain"]
                match self.peertube_basedomain:
                    case self.peertube_basedomain_prod:
                        self.login_peer_tube = self.login_peer_tube_prod
                    case self.peertube_basedomain_dev:
                        self.login_peer_tube = self.login_peer_tube_dev
                    case _:
                        result["success"] = success
                        return result
                # TODO  if we use whisper even though there was at least 1 caption then we need to reuse some of this code
                response_captions_json = self.get_captions(video_id)
                has_captions = response_captions_json["total"] > 0
                if not has_captions:
                    # TODO get the basedomain from node red and send it like we send the videoId
                    # "requestApiPaths": [] somehow generalise this so that if the api paths change we don't have to change the code
                    items = self.peertube_collection.find({
                        "peertube_basedomain": self.peertube_basedomain,
                        "video_id": video_id
                    })
                    items = list(items) # according to this - https://stackoverflow.com/a/62550243/9921564
                    if items:
                        result["message"] = f"transcription and translation already in progress for video {video_id}"
                        result["success"] = True
                        return result
                    source_id = str(uuid.uuid4())
                    doc = {"source_id": source_id, "peertube_basedomain": self.peertube_basedomain, "video_id": video_id }
                    self.save_data(doc)

                    # TODO error message handling if not existing url
                    if "url" in payload:
                        result["url"] = payload["url"]
                    if "language" in payload:
                        result["language"] = payload["language"]

                    result["source_id"] = source_id
                    result["success"] = True
                    result["message"] = "Triggering whisper transcription and translation"
                    return result
                else:
                    (
                        file_name,
                        source_language_id,
                        target_language_ids,
                    ) = self.get_first_caption(video_id, response_captions_json)

                    if source_language_id in target_language_ids:
                        target_language_ids.remove(source_language_id)

                    translations_as_vtt = self.translate_captions_xml(file_name, source_language_id, target_language_ids)
                    file_name_ending = f"-{source_language_id}.vtt"
                    first_part_file_name = file_name.split(file_name_ending)[0]
            if "success" not in translations_as_vtt or translations_as_vtt["success"]:
                for target_language_id in translations_as_vtt:
                    if translations_as_vtt[target_language_id]["success"]:
                        # get translations[target_language_id][sum_successful_captions]/amount_of_captions higher than a certain percentage
                        #  (perhaps configurable) if yes upload if not send an error log TODO
                        target_file_name = (
                            f"{first_part_file_name}-{target_language_id}.vtt"
                        )
                        translations_as_vtt[target_language_id]["payload"].save(target_file_name)
                        self.upload_caption(target_file_name, video_id, target_language_id)
                        os.remove(target_file_name) # TODO this should be in a finally block and we should check first if it exists and then delete
                                                    # or check what happens if we just delete without checking maybe that's ok
                    success = True
            else:
                logging.info("no translation was successful")

        except HTTPError as http_error:
            logging.exception("HTTPError Exception thrown")
        except NoTargetLanguagesLeftToTranslateException as e2:
            result["message"] = str(e2)
            success = True
        except Exception:
            logging.exception("Exception thrown")

        result["success"] = success
        return result

    def get_captions(self, video_id):
        # captions api of peertube to get the path of the caption
        headers = {"Authorization": "Bearer " + self.login_peer_tube.get_access_token()}
        response_captions = requests.get(
            f"{self.peertube_basedomain}/api/v1/videos/{video_id}/captions",
            headers=headers
        )
        if response_captions.status_code != 200:
            response_captions.raise_for_status()
        response_captions_json = response_captions.json()
        return response_captions_json

    # Get the first caption for now if there is no video language and more than 1 caption
    # If there is exactly one we use it without checking the video language
    # we could also get the language of the video via whisper (and fetch it here from the db)
    # might make sense to change this to use English if it exists 
    # or to let the user(on per video bases)/provider(per channel bases) choose a default
    def get_first_caption(self, video_id, response_captions_json):
        # get the caption file from the path, the default is the first 
        # and if we have a language for the video we take the caption of that language (if it exists!)
        caption_path = response_captions_json["data"][0]["captionPath"]
        source_language_id = response_captions_json["data"][0]["language"]["id"]

        target_language_ids = []
        for language_section in self.supported_language_ids:
            if language_section["code"] == source_language_id:
                target_language_ids += language_section["targets"]
        if response_captions_json["total"] > 1:
            # if there is more than one caption, use the language of the video
            headers = {"Authorization": "Bearer " + self.login_peer_tube.get_access_token()}
            response_video = requests.get(
                f"{self.peertube_basedomain}/api/v1/videos/{video_id}",
                headers=headers
            )
            if response_video.status_code != 200:
                response_video.raise_for_status()
            video_language_id = response_video.json()["language"]["id"]
            for caption_info in response_captions_json["data"]:
                caption_language_id = caption_info["language"]["id"]
                if (
                    video_language_id is not None
                    and caption_language_id == video_language_id
                ):
                    caption_path = caption_info["captionPath"]
                    source_language_id = caption_language_id
                # removing existing captions from target languages so they are not overwritten
                if caption_language_id in target_language_ids:
                    target_language_ids.remove(caption_language_id)
            if not target_language_ids:
                raise NoTargetLanguagesLeftToTranslateException(
                    "Captions already exist for all languages"
                ) # the only reason I'm raising an error here is because I can't just return nothing (I have three variables in the return)
        headers = {"Authorization": "Bearer " + self.login_peer_tube.get_access_token()}
        response_caption = requests.get(f"{self.peertube_basedomain}{caption_path}", headers=headers)
        if response_caption.status_code != 200:
            response_caption.raise_for_status()

        file_name = caption_path.split("/")[-1]
        # save the caption in a vtt object
        with open(file_name, "wb") as caption_file:
            caption_file.write(response_caption.content)

        return file_name, source_language_id, target_language_ids

    def upload_caption(self, target_file_name, video_id, caption_language):
        headers = {"Authorization": "Bearer " + self.login_peer_tube.get_access_token()}
        endpoint = f"/api/v1/videos/{video_id}/captions/{caption_language}"
        url = f"{self.peertube_basedomain}{endpoint}"
        with open(target_file_name, "rb") as file:
            files = {"captionfile": (target_file_name, file, "text/vtt; charset=utf-8")}
            response = requests.put(url, headers=headers, files=files)
            if response.status_code != 204:
                response.raise_for_status()

    def set_up_database(self):
        load_dotenv() # do we need this, doesn't it happen anyway?
        
        config = {
            'username': os.getenv('MONGO_USERNAME'),
            'password': os.getenv('MONGO_PASSWORD'),
            'host': os.getenv('MONGO_HOST'),
            'database': os.getenv('MONGO_DATABASE'),
            'port': int(os.getenv('MONGO_PORT'))
        }
        db = self.connector.database['mongo'].MongoDB(config)
        db.connect()
        list_of_collections = db.collection_names()
         # drop the collection in case this is started after a crash
        if TRANSCRIBE_AND_TRANSLATE_COLLECTION in list_of_collections:
            collection = db.get_collection(TRANSCRIBE_AND_TRANSLATE_COLLECTION)
            collection.drop()
        collection = db.get_collection(TRANSCRIBE_AND_TRANSLATE_COLLECTION)
        return collection

    def save_data(self, doc):
        result = self.peertube_collection.insert_one(doc)
        return result.inserted_id


    def get_data_by_source_id(self, source_id):
        query = {"source_id": source_id}
        source_id_doc = list(self.peertube_collection.find(query))[0] # according to this - https://stackoverflow.com/a/62550243/9921564
        return source_id_doc

    def adapter_name(self):
        return 'peertube_transcribe_and_translate'



# send first to peertube adapter to check if there are no captions only then continue to whisper?
# 

## future test cases (not yet implemented) :
# no cpations
# one caption
#   same language as video
#       the caption was generated via transcrption
#       the caption was uploaded by the user
#       the user chose a language to be translated from
#   different language than video
#       transcription went wrong
#       no language selected for video
#       the user chose this one caption to be translated from
# more than one caption
#   one of them is the same language as video language
#       the caption was generated via transcrption
#       the caption was uploaded by the user
#       the user chose a language to be translated from
#   none of them is the same language as video
#       transcription went wrong
#       no language selected for video
#       the user chose a language to be translated from
